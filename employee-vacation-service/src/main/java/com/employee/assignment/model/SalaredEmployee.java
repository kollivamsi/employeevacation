package com.employee.assignment.model;

import javax.persistence.Entity;

@Entity
public class SalaredEmployee extends Employee {

	public SalaredEmployee() {
		this.setType("SalaredEmployee");
		this.setVacationDays(15);
	}
	
	public SalaredEmployee(String firstName, String lastName, String emailId) {
		super(firstName, lastName, emailId, "SalaredEmployee");
		this.setVacationDays(15);
	}
	
	public SalaredEmployee(String firstName, String lastName, String emailId, String type) {
		super(firstName, lastName, emailId, type);
	}
	
}
