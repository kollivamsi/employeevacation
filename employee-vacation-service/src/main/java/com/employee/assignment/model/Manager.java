package com.employee.assignment.model;

import javax.persistence.Entity;

@Entity
public class Manager extends SalaredEmployee {

	public Manager() {
		this.setType("Manager");
		this.setVacationDays(30);
	}
	
	public Manager(String firstName, String lastName, String emailId) {
		super(firstName, lastName, emailId, "Manager");
		this.setVacationDays(30);
	}
	
}
