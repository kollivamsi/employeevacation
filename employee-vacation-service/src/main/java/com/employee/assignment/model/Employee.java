package com.employee.assignment.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "employees")
public class Employee {

	private static final int MAX_WORKDAYS = 260;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected long id;
	
	@Column(name = "first_name", nullable = false)
	protected String firstName;
	
	@Column(name = "last_name", nullable = false)
	protected String lastName;
	
	@Column(name = "email_address", nullable = false)
	protected String emailId;
	
	@Column(name = "type", nullable = false)
	protected String type;
	
	@Column(name = "vacation_days", nullable = false)
	protected float vacationDays;
	
	@Column(name = "work_days", nullable = false)
	protected int workDays = 0;
	
	public Employee() {
		
	}
	
	public Employee(String firstName, String lastName, String emailId, String type) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailId = emailId;
		this.type = type;
	}
	
	public void work(int workdays) {
		int temp = this.getWorkDays()+workdays;
		if(temp <= MAX_WORKDAYS) {
			this.setWorkDays(temp);
		}
	}
	
	public void takeVacation(float vacation) {
		float temp = this.getVacationDays()-vacation;
		if(temp>=0) {
			this.setVacationDays(temp);
		}
	}
	
}
