package com.employee.assignment.model;

import javax.persistence.Entity;

@Entity
public class HourlyEmployee extends Employee {

	public HourlyEmployee() {
		this.setType("HourlyEmployee");
		this.setVacationDays(10);
	}
	
	public HourlyEmployee(String firstName, String lastName, String emailId) {
		super(firstName, lastName, emailId, "HourlyEmployee");
		this.setVacationDays(10);
	}
	
}
