package com.employee.assignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import com.employee.assignment.model.HourlyEmployee;
import com.employee.assignment.model.Manager;
import com.employee.assignment.model.SalaredEmployee;
import com.employee.assignment.repository.EmployeeRepository;

@SpringBootApplication
public class Application {
	
	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(Application.class, args);
		
		EmployeeRepository employeeRepository = context.getBean(EmployeeRepository.class);
		
		String[][] data = {
				{"john","smith","john@gmail.com"},
				{"vamsi","krishna","vamsi@gmail.com"},
				{"smith","jonus","smith@gmail.com"},
				{"hemanth","kumar","hemanth@gmail.com"},
				{"bhargav","krishna","bhargav@gmail.com"},
				{"pavan","kiru","pavan@gmail.com"},
				{"ashok","latha","ashok@gmail.com"},
				{"kumar","kiran","kumar@gmail.com"},
				{"vinay","bharath","vinay@gmail.com"},
				{"tarun","kolli","tarun@gmail.com"},
		};
		
		for (String[] strings : data) {
			HourlyEmployee hourlyEmployee = new HourlyEmployee(strings[0], strings[1], strings[2]);
			employeeRepository.save(hourlyEmployee);
			
			SalaredEmployee salaredEmployee = new SalaredEmployee(strings[1], strings[0], strings[2]);
			employeeRepository.save(salaredEmployee);
			
			Manager manager = new Manager(strings[0], strings[1], strings[2]);
			employeeRepository.save(manager);
		}
	}
}
