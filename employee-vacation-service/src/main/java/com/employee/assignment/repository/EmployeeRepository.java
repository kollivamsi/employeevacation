package com.employee.assignment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employee.assignment.model.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>{

}
