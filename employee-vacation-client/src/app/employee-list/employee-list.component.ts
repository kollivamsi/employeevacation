import { EmployeeDetailsComponent } from './../employee-details/employee-details.component';
import { Observable } from "rxjs";
import { EmployeeService } from "./../employee.service";
import { Employee } from "./../employee";
import { Component, OnInit, Inject } from "@angular/core";
import { Router } from '@angular/router';

@Component({
  selector: "app-employee-list",
  templateUrl: "./employee-list.component.html",
  styleUrls: ["./employee-list.component.css"]
})
export class EmployeeListComponent implements OnInit {
  employees: Observable<Employee[]>;

  constructor(@Inject('Window') window: Window, private employeeService: EmployeeService,
    private router: Router) {}

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.employees = this.employeeService.getEmployeesList();
  }

  employeeDetails(id: number){
    this.router.navigate(['details', id]);
  }

  work(id: number){
    var workDays = parseInt(window.prompt('Enter work days:'));
    this.employeeService.work(id, workDays).subscribe(data => {
      console.log(data)
      this.reloadData();
    }, error => console.log(error));
  }

  takeVacation(id: number){
    var vacationDays = parseFloat(window.prompt('Enter vacation days:'));
    this.employeeService.takeVacation(id, vacationDays).subscribe(data => {
      console.log(data)
      this.reloadData();
    }, error => console.log(error));
  }

}
