import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private baseUrl = 'http://localhost:8080/employees';

  constructor(private http: HttpClient) { }

  getEmployee(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  work(id: number, workDays: number): Observable<any> {
    return this.http.put(`${this.baseUrl}/${id}/work/?workdays=${workDays}`, {});
  }

  takeVacation(id: number, vacationDays: number): Observable<any> {
    return this.http.put(`${this.baseUrl}/${id}/take_vacation/?vacationdays=${vacationDays}`, {});
  }

  getEmployeesList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
